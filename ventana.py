import tkinter as tk
from tkinter import Button, Entry, Frame, Label, Scrollbar, ttk
from tkinter import messagebox as mb
from tkinter.constants import END, LEFT, RIGHT, VERTICAL, Y
import base_d

class registro(Frame):
    def __init__(self, master = None):
        self.base = base_d.base()
        super().__init__(master,width=760,height=260)
        self.master = master
        self.pack()
        self.crear_widgets()
        self.mostrar_todo()
        
        
    def crear_widgets(self):

        ## para hcer el frame de los botones modificar y eliminar
        frame1 = Frame(self,bg='#99d3e5')
        frame1.place(x=0,y=0,width=135,height=259)

        ##botones
        #self.btnNuevo = Button(frame1,text="Guardar Cambios",bg="#1c6694",fg="white",command=self.guardar_cambios)
        #self.btnNuevo.place(x=5,y=50,height=30,width=120)
        self.btnModificar = Button(frame1,text="Modificar",bg="#1c6694",fg="white",command=self.modificar)
        self.btnModificar.place(x=25,y=90,height=30,width=80)
        self.btnEliminar= Button(frame1,text="Eliminar",bg="#1c6694",fg="white",command=self.eliminar)
        self.btnEliminar.place(x=25,y=130,height=30,width=80)

        ## para hcer el frame2 de los botones y etiquetas
        frame2 = Frame(self,bg='#9cc6d2')
        frame2.place(x=140,y=0,width=190,height=259)

        ##etiquetas
        label1 = Label(frame2,text="Placa",font=("times new roman",14),bg='#9cc6d2')
        label1.place(x=3,y=5)
        self.placacarga=tk.StringVar()
        self.entryplaca = Entry(frame2,textvariable=self.placacarga)
        self.entryplaca.place(x=3,y=30,width=60,height=20)

        label2 = Label(frame2,text="Color",font=("times new roman",14),bg='#9cc6d2')
        label2.place(x=3,y=55)
        self.colorcarga=tk.StringVar()
        self.entrycolor = Entry(frame2,textvariable=self.colorcarga)
        self.entrycolor.place(x=3,y=80,width=120,height=20)


        label3 = Label(frame2,text="Modelo",font=("times new roman",14),bg='#9cc6d2')
        label3.place(x=3,y=105)
        self.modelocarga=tk.StringVar()
        self.entrymodelo = Entry(frame2, textvariable=self.modelocarga)
        self.entrymodelo.place(x=3,y=130,width=120,height=20)

        label4 = Label(frame2,text="Año",font=("times new roman",14),bg='#9cc6d2')
        label4.place(x=3,y=155)
        self.añocarga=tk.StringVar()
        self.entryaño = Entry(frame2,textvariable=self.añocarga)
        self.entryaño.place(x=3,y=180,width=60,height=20)

        ##botones
        self.btnNuevo = Button(frame2,text="Guardar",bg="green",fg="white",command=self.nuevo)
        self.btnNuevo.place(x=5,y=220,height=30,width=70)
        self.btncancelar = Button(frame2,text="cancelar",bg="#cd2431",fg="white",command=self.cancelar)
        self.btncancelar.place(x=75,y=220,height=30,width=70)

        #tabla o treeview
        frame3 = Frame(self,bg='yellow')
        frame3.place(x=330,y=0,width=420,height=260)

        self.grid = ttk.Treeview(frame3,height=4,columns=("#0","#1","#2","#3"))
        self.grid.bind("<Double-Button-1>",self.doubleclick)
        self.grid.column("#0",width=110,anchor="center")
        self.grid.column("#1",width=110,anchor="center")
        self.grid.column("#2",width=110,anchor="center")
        self.grid.column("#3",width=100,anchor="center")

        self.grid.heading("#0",text="Placa",anchor="center")
        self.grid.heading("#1",text="color",anchor="center")
        self.grid.heading("#2",text="Modelo",anchor="center")
        self.grid.heading("#3",text="Año",anchor="center")
        self.grid.pack(side=LEFT,fill=Y)
        self.grid['selectmode']='browse'

        ##creando scroll
        sb = Scrollbar(frame3,orient=VERTICAL)
        sb.pack(side=RIGHT,fill=Y)
        self.grid.config(yscrollcommand=sb.set)       
        sb.config(command=self.grid.yview) 

    def nuevo(self):
        
        datos=(self.placacarga.get(), self.colorcarga.get(),self.modelocarga.get(),self.añocarga.get())
        tabla = "vehiculo"
        self.base.insertar(datos,tabla)
        if datos == "":
            mb.showinfo("Información", "Los datos fueron cargados")
        else:    
            mb.showinfo("Infomacion","los datos no pueden estar vacios")
            self.placacarga.set("")
            self.colorcarga.set("")
            self.modelocarga.set("")
            self.añocarga.set("")
            self.mostrar_todo()
    
    def mostrar_todo(self):

        tabla = "vehiculo"
        delete = self.grid.get_children()
        for elemento in delete: #para que no se repitan los elementos en el treeview
            self.grid.delete(elemento)
        rows= self.base.mostrar(tabla)
        for row in rows:
            self.grid.insert('',END,text=row[0],values=(row[1],row[2],row[3]))

    def modificar(self):

        dato1 = ((self.grid.item(self.grid.selection())["text"]))
        
        print(dato1)


        tabla = "vehiculo"
        
        self.mostrar_todo()

        d={"placa":self.placacarga.get(),
            "color":self.colorcarga.get(),
            "modelo":self.modelocarga.get(),
            "año":self.añocarga.get()}
        print(d)
        #n_dato,tabla,campo,dato1
        #sql=f"UPDATE {tabla} SET {campo}='{n_dato}' WHERE {campo}='{dato1}'"
        #d,tabla,campo,dato1
         

        var=(f",".join("{} = '{}'".format(k,v) for k,v in d.items()))

        #    print(+'='','.join(d))
            
        self.base.modificacion(var,tabla,dato1)
        self.mostrar_todo()

    def eliminar(self):
        selected = self.grid.focus() #para imprimir el id
        clave=self.grid.item(selected,'text') #para imprimir la fila completa
        tabla = "vehiculo"
        campo = "placa"

        if clave == '':
            mb.showwarning("Informacion",'debes seleccionar un elemento')
            
        else:
            self.grid.item(selected,'values')
            r = mb.askquestion("Eliminar","Deseas eliminar el elemento seleccionado")
            if r == mb.YES:
                n = self.base.borrar(clave,tabla,campo)
                self.mostrar_todo()
                if n == 1:
                    mb.showinfo("Eliminar","Elemento eliminado correctamente")

    def doubleclick(self,event):
        self.clavevieja=str(self.grid.item(self.grid.selection())["values"][0])
        self.entryplaca.delete(0,END)
        self.entrycolor.delete(0,END)
        self.entrymodelo.delete(0,END)
        self.entryaño.delete(0,END) 
        self.btnNuevo["state"]="disable"
        self.btnModificar["state"]="normal"
        self.entryplaca.insert(0,str(self.grid.item(self.grid.selection())["text"]))
        self.entrycolor.insert(0,str(self.grid.item(self.grid.selection())["values"][0]))
        self.entrymodelo.insert(0,str(self.grid.item(self.grid.selection())["values"][1]))
        self.entryaño.insert(0,str(self.grid.item(self.grid.selection())["values"][2]))

    def cancelar(self):
        self.entryplaca.delete(0,END)
        self.entrycolor.delete(0,END)
        self.entrymodelo.delete(0,END)
        self.entryaño.delete(0,END) 
        self.btnNuevo["state"]="normal"
def main():
    ventana=tk.Tk()
    ventana.title('REGISTRO DE VEHICULOS')
    app = registro(ventana)
    app.mainloop()

if __name__ == "__main__":
    main()